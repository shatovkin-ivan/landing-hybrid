'use strict'

window.addEventListener('DOMContentLoaded', () => {
    const telFields = document.querySelectorAll('input[name="tel"]')

    telFields.forEach(field => {
        if (field) {
            field.addEventListener("input", () => {
                if (field.value.length) {
                    if (field.value.match(/\d/g).length < 11) {
                        field.closest(".field-wrap").classList.add("error")
                        field.setAttribute("formnovalidate", "")
                        field.closest('.field-wrap').querySelector('.error-text').textContent = "Заполните поле"
                    } else {
                        field.closest(".field-wrap").classList.remove("error")
                        field.removeAttribute("formnovalidate", "")
                        field.closest('.field-wrap').querySelector('.error-text').textContent = ''
                    }
                }
            })

            const telMask = new Inputmask("+7 (999) 999-99-99")
            telMask.mask(field)
        }
    })

    const textAreaFields = document.querySelectorAll('textarea[name="message"]')
    const mailFields = document.querySelectorAll('input[name="email"]')
    const nameInputs = document.querySelectorAll('input[name="name"]')

    function setFieldMaxLength(fields, length) {
        fields.forEach(field => { 
            if (field) {
                field.setAttribute('maxlength', length)
            }
        })
    }

    setFieldMaxLength(textAreaFields, 250)
    setFieldMaxLength(mailFields, 64)
    setFieldMaxLength(nameInputs, 32)

    mailFields.forEach(field => {
        if (field) {
            field.addEventListener('input', () => {
                const regular = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
                if (!field.value.match(regular)) {
                    field.closest(".field-wrap").classList.add("error")
                    field.setAttribute("formnovalidate", "")
                    field.closest('.field-wrap').querySelector('.error-text').textContent = "Заполните поле"
                } else {
                    field.closest(".field-wrap").classList.remove("error")
                    field.removeAttribute("formnovalidate", "")
                    field.closest('.field-wrap').querySelector('.error-text').textContent = ''
                }
            })
        }
    })
    

    const openModalBtn = document.querySelectorAll('.open-modal')

    openModalBtn.forEach(btn => {
        if (btn) {
            btn.addEventListener('click', e => {
                openModal(e)
            })
        }
    })

    const closeBtns = document.querySelectorAll('.modal__close')

    closeBtns.forEach(closeBtn => {
        if (closeBtn) {
            closeBtn.addEventListener('click', e => {
                closeModal(e)
            })
        }
    })

    const modals = document.querySelectorAll('.modal')
    
    modals.forEach(modal => {
        if (modal) {
            modal.addEventListener('click', e => {
                if (e.target === modal) {
                    closeModal(e)
                }
            })
        }
    })

    const animateTargets = document.querySelectorAll('.animateElement')
    
    animateTargets.forEach(animateTarget => {
        if (animateTarget) {
            window.addEventListener('scroll', () => {
                if (window.pageYOffset > animateTarget.getBoundingClientRect().top) {
                    animateTarget.classList.add('animated')
                } 
                else {
                    animateTarget.classList.remove('animated')
                }
            })
        }
    })

    function getScrollbarWidth() {
        const outer = document.createElement('div')
        outer.style.visibility = 'hidden'
        outer.style.overflow = 'scroll'
        outer.style.msOverflowStyle = 'scrollbar'
        document.body.appendChild(outer)
      
        const inner = document.createElement('div')
        outer.appendChild(inner)
      
        const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth)
      
        outer.parentNode.removeChild(outer)

        return scrollbarWidth
    }

    function openModal(event) {
        const body = document.querySelector('body')
        const btnAttr = event.target.getAttribute('data-btn')
        const desiredModal = document.querySelector(`.modal[data-modal=${btnAttr}]`)
        if (desiredModal) {
            desiredModal.classList.add('active')
            body.classList.add('hidden')
            body.style.paddingRight = getScrollbarWidth() + 'px'
        }   
    }

    function closeModal(event) {
        const body = document.querySelector('body')
        if (event.target.classList.contains('active')) {
            event.target.classList.remove('active')
            body.classList.remove('hidden')
            body.style.paddingRight = 0 + 'px'
        } 
        if (event.target.closest('.modal').classList.contains('active')) {
            event.target.closest('.modal').classList.remove('active')
            body.classList.remove('hidden')
            body.style.paddingRight = 0 + 'px'
        }
    }
})