let project_folder = "dist";
let source_folder = "src";

let path = {
    build: {
        html: project_folder + "/",
        css: project_folder + "/css/",
        cssLibs: project_folder + "/css/libs",
        js: project_folder + "/js/",
        jsLibs: project_folder + "/js/libs",
        img: project_folder + "/images/",
        fonts: project_folder + "/fonts/",
    },
    src: {
        html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],
        css: source_folder + "/scss/style.scss",
        cssLibs: source_folder + "/css/libs/*.css",
        js: source_folder + "/js/main.js",
        jsLibs: source_folder + "/js/libs/*.js",
        img: source_folder + "/images/**/*.{jpg,png,svg,gif,ico,webp}",
        fonts: source_folder + "/fonts/*.{eot,svg,ttf,woff,woff2}",
    },
    watch: {
        html: source_folder + "/**/*.html",
        css: source_folder + "/scss/**/*.scss",
        cssLibs: source_folder + "/css/libs/*.css",
        js: source_folder + "/js/**/*.js",
        jsLibs: source_folder + "/js/libs/*.js",
        img: source_folder + "/images/**/*.{jpg,png,svg,gif,ico,webp}",
        fonts: source_folder + "/fonts/*.{eot,svg,ttf,woff,woff2}"
    },
    clean: "./" + project_folder + "/"
}

let { src, dest } = require('gulp'),
    gulp = require('gulp'),
    browsersync = require("browser-sync").create(),
    fileinclude = require("gulp-file-include"),
    del = require("del"),
    scss = require('gulp-sass')(require('sass')),
    autoprefixer = require("gulp-autoprefixer"),
    group_media = require("gulp-group-css-media-queries"),
    clean_css = require("gulp-clean-css"),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify-es").default,
    babel = require("gulp-babel");

function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: "./" + project_folder + "/"
        },
        port: 3000,
        notify: false
    })
}

function html() {
    return src(path.src.html)
        .pipe(fileinclude())
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream())
}

function css() {
    return src(path.src.css)
        .pipe(
            scss({
                outputStyle: "expanded"
            })
        )
        .pipe(
            group_media()
        )
        .pipe(
            autoprefixer({
                overrideBrowserslist: ["last 5 version"],
                cascade: true
            })
        )
        .pipe(dest(path.build.css))
        .pipe(clean_css())
        .pipe(
            rename({
                extname: ".min.css"  
            })
        )
        .pipe(dest(path.build.css))
        .pipe(browsersync.stream())
}

function css_libs() {
    return src(path.src.cssLibs)
        .pipe(dest(path.build.cssLibs))
        .pipe(browsersync.stream())
}

function js() {
    return src(path.src.js)
        .pipe(fileinclude())
        .pipe(
            babel({
                presets: ["@babel/env"]
            })
        )
        .pipe(dest(path.build.js))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: ".min.js"  
            })
        )
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}

function js_libs() {
    return src(path.src.jsLibs)
        .pipe(dest(path.build.jsLibs))
        .pipe(browsersync.stream())
}

function fonts() {
    return src(path.src.fonts)
        .pipe(dest(path.build.fonts))
        .pipe(browsersync.stream())
}

function imgs() {
    return src(path.src.img)
        .pipe(dest(path.build.img))
        .pipe(browsersync.stream())
}

function watchFiles(params) {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.css], css);
    gulp.watch([path.watch.cssLibs], css_libs);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.jsLibs], js_libs);
    gulp.watch([path.watch.fonts], fonts);
    gulp.watch([path.watch.img], imgs);
}

function clean(params) {
    return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, js_libs, css, css_libs, html, fonts, imgs))
let watch = gulp.parallel(build, watchFiles, browserSync)

exports.js = js;
exports.jsLibs = js_libs;
exports.css = css;
exports.cssLibs = css_libs;
exports.html = html;
exports.fonts = fonts;
exports.imgs = imgs;
exports.build = build;
exports.watch = watch;
exports.default = watch;
